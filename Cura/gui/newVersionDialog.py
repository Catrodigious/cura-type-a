__copyright__ = "Copyright (C) 2013 David Braam Released under terms of the AGPLv3 License; additional contributions Copyright (C) 2016 Type A Machines released under terms of the AGPLv3 License"

import wx
import wx.lib.agw.hyperlink as hl
from Cura.util import version
from Cura.util import profile

class newVersionDialog(wx.Dialog):
	def __init__(self, parent):
		super(newVersionDialog, self).__init__(parent, title="Welcome to the New Version!", style=wx.DEFAULT_DIALOG_STYLE | wx.STAY_ON_TOP)

		p = wx.Panel(self)
		self.panel = p
		s = wx.BoxSizer()
		self.SetSizer(s)
		s.Add(p, flag=wx.ALL, border=15)
		s = wx.BoxSizer(wx.VERTICAL)
		p.SetSizer(s)
		
		# Fonts
		titleFont = wx.Font(18, wx.DEFAULT, wx.NORMAL, wx.NORMAL)
		headerFont = wx.Font(14, wx.DEFAULT, wx.NORMAL, wx.NORMAL)
		textFont = wx.Font(14, wx.DEFAULT, wx.NORMAL, wx.NORMAL)
		
		# Title text
		title = wx.StaticText(p, -1, 'Cura Type A - ' + version.getVersion())
		title.SetFont(titleFont)
		versionForked = wx.StaticText(p, -1, 'Based On Daid/Ultimaker\'s Cura v15.02.')
		versionForked.SetFont(headerFont)
		s.Add(title, flag=wx.ALIGN_CENTRE|wx.EXPAND|wx.BOTTOM, border=5)
		s.Add(versionForked)
		s.Add(wx.StaticLine(p), flag=wx.EXPAND|wx.TOP|wx.BOTTOM, border=5)

		newMaterialProfiles = wx.StaticText(p, -1, "New Material Profiles")
		newMaterialProfiles.SetFont(titleFont)
		s.Add(newMaterialProfiles, flag=wx.TOP | wx.BOTTOM, border=5)
		materials = [
			wx.StaticText(p, -1, "* Introducing Type A Machines Everyday PLA and Performance PLA"),
			wx.StaticText(p, -1, "* Material profile vendor list updated and re-organized"),
			wx.StaticText(p, -1, "* 78 materials total")
		]
		for material in materials:
			material.SetFont(textFont)
			s.Add(material, flag=wx.BOTTOM, border=5)
		hyperlink = hl.HyperLinkCtrl(p, -1, "* Complete list here", URL='http://bit.ly/2bm9Eck')
		hyperlink.SetFont(textFont)
		s.Add(hyperlink, flag=wx.BOTTOM, border=20)
	
		# New in this version
		newHere = wx.StaticText(p, -1, "New Features and Improvements")
		newHere.SetFont(titleFont)
		degree_sign = u'\N{DEGREE SIGN}'
		s.Add(newHere, flag=wx.TOP | wx.BOTTOM, border=5)

		featuresAndEnhancements = [
			wx.StaticText(p, -1, "* Absolute Dimensions\n - Specifying infill by millimeter rather than percentage"),
			wx.StaticText(p, -1, "* 3D Cubic Structure\n - Delivering axis-independent interior structure"),
			wx.StaticText(p, -1, "* New Concentric Infill pattern introduced"),
			wx.StaticText(p, -1, "* Infill Visualizer\n - Toggles the display of infill in-place"),
			wx.StaticText(p, -1, "* 'Infill Prints After Perimeter' setting defaults to True"),
			wx.StaticText(p, -1, "* Expert Mode side panel now displays:\n - Extrusion Width, Number of Shells, Infill and Flow Percentage"),
			wx.StaticText(p, -1, "* Cmd/Ctrl-P now brings up the \"Send to Printer\" dialog"),
			wx.StaticText(p, -1, "* Tag added to GCode files including which profile was used to generate the GCode"),
			wx.StaticText(p, -1, "* User Notification for available Software Updates is now included"),
		]
		
		for item in featuresAndEnhancements:
			item.SetFont(textFont)
			s.Add(item, flag=wx.BOTTOM | wx.EXPAND, border=15)
		
		# Bug fixes
		issuesAddressed = wx.StaticText(p, -1, "Bugfixes")
		issuesAddressed.SetFont(titleFont)
		degree_sign = u'\N{DEGREE SIGN}'
		s.Add(issuesAddressed, flag=wx.TOP | wx.BOTTOM, border=5)
		issues = [
			wx.StaticText(p, -1, "* Custom start/end GCode no longer ignored under some conditions"),
			wx.StaticText(p, -1, "* Some dialog window issues addressed")
		]
		for issue in issues:
			issue.SetFont(textFont)
			s.Add(issue, flag=wx.BOTTOM, border=5)
		
		
		s.Add(wx.StaticLine(p), flag=wx.EXPAND|wx.TOP|wx.BOTTOM, border=5)
		button = wx.Button(p, -1, 'OK')
		button.Bind(wx.EVT_BUTTON, self.OnOk)
		s.Add(button, flag=wx.TOP|wx.ALIGN_CENTRE | wx.ALL, border=5)

		self.Fit()
		self.Centre()
	
	
	def addMaterialList(self, list, p, s):
	
		leftBoxSizer = wx.BoxSizer(wx.VERTICAL)
		rightBoxSizer = wx.BoxSizer(wx.VERTICAL)
		
		gs = wx.GridBagSizer(5, 5)
		row = 0
		column = 0

		# Fonts
		titleFont = wx.Font(14, wx.DEFAULT, wx.NORMAL, wx.BOLD)
		headerFont = wx.Font(14, wx.DEFAULT, wx.NORMAL, wx.NORMAL)
		textFont = wx.Font(14, wx.DEFAULT, wx.NORMAL, wx.NORMAL)
		addCount = 0

		for label, materials in list.items():
			if len(materials) > 3 and column > 0:
				column += 1
				row = 0
			materialLabel = wx.StaticText(p, -1, str(label))
			materialLabel.SetFont(titleFont)
			gs.Add(materialLabel, pos=(row, column), flag=wx.RIGHT, border=5)
			row += 1
			addCount += 1
			for material in materials:
				material = wx.StaticText(p, -1, str(material))
				gs.Add(material, pos=(row, column), flag=wx.RIGHT, border=5)
				row += 1
			row += 1
			if addCount == 3:
				column += 1
				row = 0
				addCount = 0

		s.Add(gs)

	def addMaterial(self, s, material, materialUpdates):
		# Fonts
		titleFont = wx.Font(14, wx.DEFAULT, wx.NORMAL, wx.BOLD)
		headerFont = wx.Font(14, wx.DEFAULT, wx.NORMAL, wx.NORMAL)
		textFont = wx.Font(13, wx.DEFAULT, wx.NORMAL, wx.NORMAL)

		material.SetFont(headerFont)
		s.Add(material, flag=wx.TOP, border=5)
		for update in materialUpdates:
			update.SetFont(textFont)
			s.Add(update, flag=wx.TOP | wx.LEFT, border=5)

	def OnOk(self, e):
		self.Close()
		
	def OnClose(self, e):
		self.Destroy()
