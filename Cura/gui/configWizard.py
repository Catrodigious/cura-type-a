__copyright__ = "Copyright (C) 2013 David Braam Released under terms of the AGPLv3 License; additional contributions Copyright (C) 2016 Type A Machines released under terms of the AGPLv3 License"

import os
import webbrowser
from wx.lib.pubsub import pub
import wx.lib.agw.hyperlink as hl
import threading
import time
import math
import wx
import re
import wx.wizard
import ConfigParser as configparser
try:
    from io import BytesIO
except ImportError:
    from StringIO import StringIO as BytesIO

from Cura.gui import printWindow
from Cura.util import profile
from Cura.util import gcodeGenerator
from Cura.util import resources
from Cura.util import printerConnect
from Cura.util import version

class InfoBox(wx.Panel):
	def __init__(self, parent):
		super(InfoBox, self).__init__(parent)
		self.SetBackgroundColour('#FFFF80')

		self.sizer = wx.GridBagSizer(5, 5)
		self.SetSizer(self.sizer)

		self.attentionBitmap = wx.Bitmap(resources.getPathForImage('attention.png'))
		self.errorBitmap = wx.Bitmap(resources.getPathForImage('error.png'))
		self.readyBitmap = wx.Bitmap(resources.getPathForImage('ready.png'))
		self.busyBitmap = [
			wx.Bitmap(resources.getPathForImage('busy-0.png')),
			wx.Bitmap(resources.getPathForImage('busy-1.png')),
			wx.Bitmap(resources.getPathForImage('busy-2.png')),
			wx.Bitmap(resources.getPathForImage('busy-3.png'))
		]

		self.bitmap = wx.StaticBitmap(self, -1, wx.EmptyBitmapRGBA(24, 24, red=255, green=255, blue=255, alpha=1))
		self.text = wx.StaticText(self, -1, '')
		self.extraInfoButton = wx.Button(self, -1, 'i', style=wx.BU_EXACTFIT)
		self.sizer.Add(self.bitmap, pos=(0, 0), flag=wx.ALL, border=5)
		self.sizer.Add(self.text, pos=(0, 1), flag=wx.TOP | wx.BOTTOM | wx.ALIGN_CENTER_VERTICAL, border=5)
		self.sizer.Add(self.extraInfoButton, pos=(0,2), flag=wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL, border=5)
		self.sizer.AddGrowableCol(1)

		self.extraInfoButton.Show(False)

		self.extraInfoUrl = ''
		self.busyState = None
		self.timer = wx.Timer(self)
		self.Bind(wx.EVT_TIMER, self.doBusyUpdate, self.timer)
		self.Bind(wx.EVT_BUTTON, self.doExtraInfo, self.extraInfoButton)
		self.timer.Start(100)

	def SetInfo(self, info):
		self.SetBackgroundColour('#FFFF80')
		self.text.SetLabel(info)
		self.extraInfoButton.Show(False)
		self.Refresh()

	def SetError(self, info, extraInfoUrl):
		self.extraInfoUrl = extraInfoUrl
		self.SetBackgroundColour('#FF8080')
		self.text.SetLabel(info)
		self.extraInfoButton.Show(True)
		self.Layout()
		self.SetErrorIndicator()
		self.Refresh()

	def SetAttention(self, info):
		self.SetBackgroundColour('#FFFF80')
		self.text.SetLabel(info)
		self.extraInfoButton.Show(False)
		self.SetAttentionIndicator()
		self.Layout()
		self.Refresh()

	def SetBusy(self, info):
		self.SetInfo(info)
		self.SetBusyIndicator()

	def SetBusyIndicator(self):
		self.busyState = 0
		self.bitmap.SetBitmap(self.busyBitmap[self.busyState])

	def doExtraInfo(self, e):
		webbrowser.open(self.extraInfoUrl)

	def doBusyUpdate(self, e):
		if self.busyState is None:
			return
		self.busyState += 1
		if self.busyState >= len(self.busyBitmap):
			self.busyState = 0
		self.bitmap.SetBitmap(self.busyBitmap[self.busyState])

	def SetReadyIndicator(self):
		self.busyState = None
		self.bitmap.SetBitmap(self.readyBitmap)

	def SetErrorIndicator(self):
		self.busyState = None
		self.bitmap.SetBitmap(self.errorBitmap)

	def SetAttentionIndicator(self):
		self.busyState = None
		self.bitmap.SetBitmap(self.attentionBitmap)


class InfoPage(wx.wizard.WizardPageSimple):
	def __init__(self, parent, title):
		wx.wizard.WizardPageSimple.__init__(self, parent)

		sizer = wx.GridBagSizer(5, 5)
		self.sizer = sizer
		self.SetSizer(sizer)
		self.rowNr = 1
		self.sizer.AddGrowableCol(0)
		
	def AddLogo(self):
		curaIcon =  wx.Bitmap(resources.getPathForImage('CuraTAMIcon.png'))
		curaIcon.SetHeight(125)
		curaIcon.SetWidth(125)
		curaIconBitmap = self.AddLogoBitmap(curaIcon)

	#	curaTAMLogo = resources.getPathForImage('TAMLogoAndText.png')
	#	self.AddImage(curaTAMLogo)
		thisVersion = version.getVersion()
		self.AddTextTagLine(thisVersion)

	def AddHyperlink(self, text, url):
		hyper1 = hl.HyperLinkCtrl(self, -1, text, URL=url)
		font = wx.Font(pointSize=12, family = wx.DEFAULT,
               style = wx.NORMAL, weight = wx.LIGHT)
		hyper1.SetFont(font)
		self.GetSizer().Add(hyper1, pos=(self.rowNr,0), span=(1, 3), flag=wx.ALIGN_CENTER_HORIZONTAL)
		self.rowNr += 1
		return hyper1
		
	# Left-aligned text
	def AddText(self, info):
		text = wx.StaticText(self, -1, info, style=wx.ALIGN_LEFT)
		font = wx.Font(pointSize=12, family = wx.DEFAULT, style=wx.NORMAL, weight=wx.LIGHT)
		text.SetFont(font)
		text.Wrap(330)
		self.GetSizer().Add(text, pos=(self.rowNr, 0), span=(1, 3), flag=wx.ALIGN_CENTRE_HORIZONTAL | wx.TOP | wx.BOTTOM, border=10)
		self.rowNr += 1
		return text
		
	# Left-aligned text
	def AddBottomAlignedText(self, info):
		text = wx.StaticText(self, -1, info, style=wx.ALIGN_BOTTOM)
		font = wx.Font(pointSize=12, family = wx.DEFAULT, style=wx.NORMAL, weight=wx.LIGHT)
		text.SetFont(font)
		text.Wrap(330)
		self.GetSizer().Add(text, pos=(self.rowNr, 0), span=(1, 3), flag=wx.ALIGN_CENTRE_HORIZONTAL | wx.TOP | wx.BOTTOM, border=10)
		self.rowNr += 1
		return text
		
		
	# Center-aligned text
	def AddCenteredText(self, info):
		text = wx.StaticText(self, -1, info, style=wx.ALIGN_CENTER)
		font = wx.Font(pointSize=12, family = wx.DEFAULT, style=wx.NORMAL, weight=wx.LIGHT)
		text.SetFont(font)
		text.Wrap(340)
		self.GetSizer().Add(text, pos=(self.rowNr, 0), span=(1, 3), flag=wx.ALIGN_CENTER_HORIZONTAL)
		self.rowNr += 1
		return text
	
	def AddTextTip(self,info):
		text = wx.StaticText(self, -1, info)
		font = wx.Font(pointSize=12, family = wx.DEFAULT, style=wx.NORMAL, weight=wx.NORMAL)
		text.SetFont(font)
		text.Wrap(340)
		self.GetSizer().Add(text, pos=(self.rowNr,0), span=(1, 2), flag=wx.ALIGN_CENTER)
		self.rowNr += 1
		return text
		
	def AddTextTagLine(self, info):
		text = wx.StaticText(self, -1, info, style=wx.ALIGN_LEFT)
		font = wx.Font(pointSize=12, family = wx.DEFAULT,
               style = wx.NORMAL, weight = wx.LIGHT)
		text.SetFont(font)
		text.Wrap(340)
		self.GetSizer().Add(text, pos=(self.rowNr, 0), span=(1, 3), flag=wx.ALIGN_CENTER_HORIZONTAL)
		self.rowNr += 1
		return text
		
	def AddTextDescription(self, info):
		text = wx.StaticText(self, -1, info, style=wx.ALIGN_LEFT)
		font = wx.Font(pointSize=12, family = wx.DEFAULT,
               style = wx.NORMAL, weight = wx.NORMAL)
		text.SetFont(font)
		text.Wrap(300)
		self.GetSizer().Add(text, pos=(self.rowNr, 0), span=(1, 3), flag=wx.ALIGN_CENTRE_HORIZONTAL)
		self.rowNr += 1
		return text
		
	def AddSeries1OptionsDescription(self, info):
		text = wx.StaticText(self, -1, info, style=wx.ALIGN_LEFT)
		font = wx.Font(pointSize=12, family = wx.DEFAULT,
               style = wx.NORMAL, weight = wx.NORMAL)
		text.SetFont(font)
		text.Wrap(200)
		self.GetSizer().Add(text, pos=(self.rowNr, 0), span=(1, 2), flag=wx.LEFT | wx.EXPAND, border=130)
		self.rowNr += 1
		return text

	def AddTextLarge(self, info):
		text = wx.StaticText(self, -1, info, style=wx.ALIGN_CENTER_HORIZONTAL)
		font = wx.Font(pointSize=12, family = wx.DEFAULT,
               style = wx.NORMAL, weight = wx.LIGHT)
		text.SetFont(font)
		text.Wrap(400)
		self.GetSizer().Add(text, pos=(self.rowNr, 0), span=(1, 3), flag=wx.ALIGN_CENTER_HORIZONTAL)
		self.rowNr += 1
		return text

	def AddTextTitle(self, info):
		text = wx.StaticText(self, -1, info, style=wx.ALIGN_RIGHT)
		font = wx.Font(pointSize=14, family = wx.DEFAULT, style = wx.NORMAL, weight = wx.NORMAL)
		text.SetFont(font)
		text.Wrap(340)
		self.GetSizer().Add(text, pos=(self.rowNr, 0), span=(1, 3), flag=wx.ALIGN_CENTRE_HORIZONTAL | wx.TOP | wx.BOTTOM, border=15)
		self.rowNr += 1
		return text
		
	def AddErrorText(self, info, red=False):
		text = wx.StaticText(self, -1, info, style=wx.ALIGN_CENTRE_HORIZONTAL)
		font = wx.Font(pointSize=12, family = wx.DEFAULT,
               style = wx.NORMAL, weight = wx.NORMAL)
		text.SetFont(font)
		if red:
			text.SetForegroundColour('Red')
		else:
			text.SetForegroundColour('Blue')
			
		self.GetSizer().Add(text, pos=(self.rowNr, 0), span=(1,2), flag=wx.ALIGN_CENTRE_HORIZONTAL)

		self.rowNr += 1
		return text
		
	def AddImage(self, imagePath):
		image = wx.Image(imagePath, wx.BITMAP_TYPE_PNG).ConvertToBitmap()
		self.GetSizer().Add(wx.StaticBitmap(self, -1, image), pos=(self.rowNr, 0), span=(1, 3), flag=wx.ALIGN_CENTRE_HORIZONTAL)
		self.rowNr += 1
		return image
		
	def AddMachineImage(self, imagePath):
		image = wx.Image(imagePath, wx.BITMAP_TYPE_PNG).ConvertToBitmap()
		self.GetSizer().Add(wx.StaticBitmap(self, -1, image), pos=(self.rowNr+3, 1), span=(1, 2), flag=wx.ALIGN_RIGHT)
		return image

	def AddLabelImage(self, imagePath):
		image = wx.Image(imagePath, wx.BITMAP_TYPE_PNG).ConvertToBitmap()
		self.GetSizer().Add(wx.StaticBitmap(self, -1, image), pos=(self.rowNr, 0), span=(1, 2), flag=wx.ALIGN_CENTER)
		self.rowNr += 1
		return image

	def AddGif(self, imagePath):
		#Loading gif 
		loadingGif = wx.animate.GIFAnimationCtrl(self, -1, imagePath)
		loadingGif.Play()
		
		self.GetSizer().Add(loadingGif, pos=(self.rowNr, 0), span=(1, 2), flag=wx.ALIGN_CENTER)
		self.rowNr += 1
		return loadingGif

	def AddSeperator(self):
		self.GetSizer().Add(wx.StaticLine(self, -1), pos=(self.rowNr, 1), span=(2, 1), flag=wx.EXPAND | wx.ALL)
		self.rowNr += 1

	def AddHiddenSeperator(self, count):
		if count < 1: 
			count = 1
		for x in range(0, count):
			self.AddText("")

	def AddInfoBox(self):
		infoBox = InfoBox(self)
		self.GetSizer().Add(infoBox, pos=(self.rowNr, 0), span=(1, 2), flag=wx.LEFT | wx.RIGHT | wx.EXPAND)
		self.rowNr += 1
		return infoBox

	def AddRadioButton(self, label, style=0):
		radio = wx.RadioButton(self, -1, label, style=wx.ALIGN_LEFT)
		font = wx.Font(pointSize=12, family = wx.DEFAULT, style = wx.NORMAL | wx.LEFT, weight=wx.NORMAL)
		radio.SetFont(font)
		self.GetSizer().Add(radio, pos=(self.rowNr, 0), span=(1, 3), flag=wx.ALIGN_CENTRE_HORIZONTAL)
		self.rowNr += 1
		return radio
		
	def AddCheckbox(self, label, checked=False):
		check = wx.CheckBox(self, -1, label)
		font = wx.Font(pointSize=12, family = wx.DEFAULT, style = wx.NORMAL, weight = wx.NORMAL)
		check.SetFont(font)
		check.SetValue(checked)
		self.GetSizer().Add(check, pos=(self.rowNr, 0), span=(1, 3), flag=wx.ALIGN_CENTRE_HORIZONTAL)
		self.rowNr += 1
		return check

	def AddMachineOptionCheckbox(self, label, checked=False):
		check = wx.CheckBox(self, -1, label)
		font = wx.Font(pointSize=12, family = wx.DEFAULT, style = wx.NORMAL, weight = wx.NORMAL)
		check.SetFont(font)
		check.SetValue(checked)
		self.GetSizer().Add(check, pos=(self.rowNr, 0), span=(1, 2), flag=wx.ALIGN_LEFT | wx.LEFT, border=130)
		self.rowNr += 1
		return check

	def AddButton(self, label):
		button = wx.Button(self, -1, str(label))
		font = wx.Font(pointSize=12, family = wx.DEFAULT, style = wx.NORMAL, weight = wx.NORMAL)
		button.SetFont(font)
		self.GetSizer().Add(button, pos=(self.rowNr, 0), span=(1, 3), flag=wx.ALIGN_CENTRE_HORIZONTAL)
		self.rowNr += 1
		return button

	def AddDualButton(self, label1, label2):
		button1 = wx.Button(self, -1, label1)
		self.GetSizer().Add(button1, pos=(self.rowNr, 0), flag=wx.RIGHT)
		button2 = wx.Button(self, -1, label2)
		self.GetSizer().Add(button2, pos=(self.rowNr, 1))
		self.rowNr += 1
		return button1, button2

	def AddTextCtrl(self, value):
		ret = wx.TextCtrl(self, -1, value, size=(200, 25))
		font = wx.Font(pointSize=12, family = wx.DEFAULT, style=wx.NORMAL, weight = wx.LIGHT)
		ret.SetFont(font)
		self.GetSizer().Add(ret, pos=(self.rowNr, 0), span=(1, 2), flag=wx.ALIGN_CENTER)
		self.rowNr += 1
		return ret

	def AddLabelTextCtrl(self, info, value):
		text = wx.StaticText(self, -1, info)
		ret = wx.TextCtrl(self, -1, value)
		font = wx.Font(pointSize=12, family = wx.DEFAULT,
		style = wx.NORMAL, weight = wx.LIGHT)
		text.SetFont(font)
		self.GetSizer().Add(text, pos=(self.rowNr, 0), span=(1, 1), flag=wx.ALIGN_RIGHT | wx.LEFT, border=75)
		self.GetSizer().Add(ret, pos=(self.rowNr, 1), span=(1, 3), flag=wx.CENTER)
		self.rowNr += 1
		return ret

	def AddTextCtrlButton(self, value, buttonText):
		text = wx.TextCtrl(self, -1, value)
		button = wx.Button(self, -1, buttonText)
		self.GetSizer().Add(text, pos=(self.rowNr, 0), span=(1, 1), flag=wx.LEFT)
		self.GetSizer().Add(button, pos=(self.rowNr, 1), span=(1, 1), flag=wx.LEFT)
		self.rowNr += 1
		return text, button

	def AddBitmap(self, bitmap):
		bitmap = wx.StaticBitmap(self, -1, bitmap)
		self.GetSizer().Add(bitmap, pos=(self.rowNr, 0), span=(1, 3), flag=wx.ALIGN_CENTRE_HORIZONTAL | wx.TOP, border=10)
		self.rowNr += 1
		return bitmap
		
	def AddLogoBitmap(self, bitmap):
		bitmap = wx.StaticBitmap(self, -1, bitmap)
		self.GetSizer().Add(bitmap, pos=(self.rowNr, 0), span=(1, 3), flag=wx.ALIGN_CENTRE_HORIZONTAL)
		self.rowNr += 1
		return bitmap
		
	def AddCheckmark(self, label, bitmap):
		check = wx.StaticBitmap(self, -1, bitmap)
		text = wx.StaticText(self, -1, label)
		self.GetSizer().Add(text, pos=(self.rowNr, 0), span=(1, 1), flag=wx.LEFT | wx.RIGHT)
		self.GetSizer().Add(check, pos=(self.rowNr, 1), span=(1, 1), flag=wx.ALL)
		self.rowNr += 1
		return check

	def AddCombo(self, options):
		combo = wx.ComboBox(self, -1, options[0], choices=options, style=wx.CB_DROPDOWN|wx.CB_READONLY)
		self.GetSizer().Add(combo, pos=(self.rowNr, 1), span=(1, 1), flag=wx.LEFT)
		self.rowNr += 1
		return combo

	def AllowNext(self):
		return True

	def AllowBack(self):
		return True

	def StoreData(self):
		pass


class FirstInfoPage(InfoPage):
	def __init__(self, parent, addNew):
		if addNew:
			super(FirstInfoPage, self).__init__(parent, _("Printer Selection"))	
		else:
			super(FirstInfoPage, self).__init__(parent, _("Welcome"))
			
		self.AddLogo()
		self.AddHiddenSeperator(5)
		self.AddTextLarge("Select 'Next' to begin configuration.")		

	def AllowBack(self):
		return False

	def StoreData(self):
		pass


class MachineSelectPage(InfoPage):
	def __init__(self, parent):
		super(MachineSelectPage, self).__init__(parent, _("Select your machine"))
		self.AddLogo()
		self.AddHiddenSeperator(1)
		self.AddTextTitle('Select Printer')
		self.AddHiddenSeperator(1)
		self.Series1_Pro_Radio = self.AddRadioButton("Series 1 Pro", style=wx.RB_SINGLE)
		self.Series1_Pro_Radio.SetValue(True)
		self.AddTextDescription(_("#10000-99999"))
		self.AddHiddenSeperator(1)
		self.Series1_Radio = self.AddRadioButton("Series 1")
		self.Series1_Radio.Bind(wx.EVT_RADIOBUTTON, self.OnSeries1)
		self.AddTextDescription(_("#1000-9999"))
		self.AddHiddenSeperator(1)
		self.Series1_Radio.Bind(wx.EVT_RADIOBUTTON, self.OnSeries1)
		self.Series1_Pro_Radio.Bind(wx.EVT_RADIOBUTTON, self.OnSeries1Pro)

	def OnSeries1(self, e):
		wx.wizard.WizardPageSimple.Chain(self, self.GetParent().TAM_select_options)
		
	def OnSeries1Pro(self, e):
		wx.wizard.WizardPageSimple.Chain(self, self.GetParent().tamReadyPage)
		
	def StoreData(self):
		allMachineProfiles = resources.getDefaultMachineProfiles()
		machSettingsToStore = {}
		n = None
		
		# loop through list of buttons to find selected
		for machineProfile in allMachineProfiles:
			if self.Series1_Radio.GetValue():
				n = re.search(r'Series1\.ini', machineProfile)
			elif self.Series1_Pro_Radio.GetValue():
				n = re.search(r'Series1Pro', machineProfile)

			if n is not None:
				machProfile = machineProfile
				cp = configparser.ConfigParser()
				cp.read(machProfile)
				if cp.has_section('machine'):
					for setting, value in cp.items('machine'):
						machSettingsToStore[setting] = value
		
		# if Series 1 Pro, load the appropriate alteration file
		alterationDirectoryList = resources.getAlterationFiles()
		if self.Series1_Pro_Radio.GetValue():
			# start/end gcode
			for filename in alterationDirectoryList:
				alterationFileExists = re.search(r'series1_hasBed', filename)
				if alterationFileExists:
					profile.setAlterationFileFromFilePath(filename)
		else:
			for filename in alterationDirectoryList:
				alterationFileExists = re.search(r'series1_noBed', filename)
				if alterationFileExists:
					profile.setAlterationFileFromFilePath(filename)
			
		if machSettingsToStore:
			for setting, value in machSettingsToStore.items():
				profile.putMachineSetting(setting, value)

class TAMSelectOptions(InfoPage):
	def __init__(self, parent):
		super(TAMSelectOptions, self).__init__(parent, _("Options and Upgrades"))
		self.AddLogo()
		
		for n in range(0,3):
			self.AddHiddenSeperator(1)
		
		# G2 extruder
		g2ExtruderImage = resources.getPathForImage('g2Extruder.png')
		self.AddImage(g2ExtruderImage)
		self.G2ExtruderCheckBox = self.AddMachineOptionCheckbox("G2 Extruder")
		self.AddSeries1OptionsDescription("If you have an extruder that's not the G2, please uncheck this option.")
		self.G2ExtruderCheckBox.SetValue(True)
		
		# Spacer
		for n in range(0,3):
			self.AddHiddenSeperator(1)

		# Heated bed
		self.HeatedBedCheckBox = self.AddMachineOptionCheckbox("Heated Bed Installed")
		self.AddSeries1OptionsDescription("The heated bed is available\nas an upgrade. Contact sales@typeamachines.com\nfor more information.")
		
		# Spacer
		for n in range(0,2):
			self.AddHiddenSeperator(1)
			
		wx.wizard.WizardPageSimple.Chain(self, self.GetParent().TAM_octoprint_config)
	
	def StoreData(self):
		# Print temp 185 for non-G2
		if not self.G2ExtruderCheckBox.GetValue():
			profile.putProfileSetting('print_temperature', 185)
		# Heated bed item population
		if self.HeatedBedCheckBox.GetValue():
			profile.putMachineSetting("has_heated_bed", "True")
		else:
			profile.putMachineSetting("has_heated_bed", "False")

class TAMReadyPage(InfoPage):
	def __init__(self, parent):
		super(TAMReadyPage, self).__init__(parent, _("Configuration Complete"))
		self.AddLogo()
		tutorialConfigScreenImgPath = resources.getPathForImage('configScreen.png')	
		tutorialConfigScreenBitmap = wx.Bitmap(tutorialConfigScreenImgPath)
		self.AddBitmap(tutorialConfigScreenBitmap)
		self.AddTextTitle(_("Configuration Complete"))
		self.AddCenteredText(_("Click 'Next' for a guided tour of\nCura Type A features."))
		self.AddHiddenSeperator(2)
		
		self.analyticsCheckbox = self.AddCheckbox("Keep Analytics On", checked=True)
		self.AddText("Cura Type A collects anonymous data in order to improve your experience.")
		
	def StoreData(self):
		if self.analyticsCheckbox.GetValue() == True:
			profile.putPreference("submit_slice_information", True)
		else:
			profile.putPreference("submit_slice_information", False)		

	def AllowBack(self):
		return False

class TAMAbsoluteInfillPage(InfoPage):
	def __init__(self, parent):
		super(TAMAbsoluteInfillPage, self).__init__(parent, _("Absolute Infill"))
		self.AddLogo()
		
		self.AddTextTitle(_("Absolute Dimensions"))
		
		absDimensionsImage = resources.getPathForImage('tutorial_absDimensions.png')
		self.AddImage(absDimensionsImage)
		
		self.AddText(_("Cura Type A 1.5 measures fill density in millimeters, not percentage. This allows engineers and operators to achieve much more precise, as well as repeatable internal geometry."))

class TAM3DInfillPage(InfoPage):
	def __init__(self, parent):
		super(TAM3DInfillPage, self).__init__(parent, _("3D Infill"))
		self.AddLogo()
		
		self.AddTextTitle("3D Internal Structures")
		
		tam3DFill = resources.getPathForImage('tutorial_3DFill.png')
		self.AddImage(tam3DFill)
		
		
		self.AddText("In addition to the aforementioned \"Absolute Dimensions\", a 3D fill pattern is available. Unlike standard repeating infill patterns, this 3D internal structure makes parts equally strong regardless of their printing orientation.")

class TAMHeatedBedWarning(InfoPage):
	def __init__(self, parent):
		super(TAMHeatedBedWarning, self).__init__(parent, _("Heated Bed Warning"))
		self.AddLogo()
		
		
		warningImagePath = resources.getPathForImage('warning.png')
		self.AddTextTitle("Heated Bed Adhesion Warning")
		warningImage =  wx.Bitmap(warningImagePath)
		warningImage.SetHeight(125)
		warningImage.SetWidth(125)
		self.AddLogoBitmap(warningImage)
		
		
		self.AddText("Always use a surface treatment with the heated bed as materials can permanently bond with glass, resulting in damage not covered under warranty. See the material manufacturer for specific surface treatment recommendations.")
	
		
class TAMOctoPrintInfo(InfoPage):
	def __init__(self, parent):
		super(TAMOctoPrintInfo, self).__init__(parent, _("Octoprint Configuration"))
		
		self.AddLogo()
		self.validSerial = False
		self.validKey = False
		self.saveInfo = False
		self.parent = parent
		self.configurationAttemptedOnce = False
		self.inputCheck = printerConnect.InputValidation()
		self.AddTextTitle("Enable Saving Directly to Series 1")	
		apiTip = resources.getPathForImage('apiTip.png')
		self.AddImage(apiTip)
		self.AddText("Enter the serial number found on the front panel of your Series 1. Then enter the printer's API key, found in the printer's web interface in settings.")
		self.AddHiddenSeperator(1)
		self.serialNumber = self.AddLabelTextCtrl("Serial Number", "")
		self.APIKey = self.AddLabelTextCtrl("API Key", "")
		self.AddHiddenSeperator(1)
		self.configurePrinterButton = self.AddButton("Configure")
		self.skipConfig = self.AddCheckbox("Skip Configuration", checked=False)
		self.errorMessageln1 = self.AddErrorText('')
		self.configurePrinterButton.Bind(wx.EVT_BUTTON, self.attemptConfiguration)
		self.skipConfig.Bind(wx.EVT_CHECKBOX, self.skipPage)
		self.configurePrinterButton.Disable()
		
		self.serialNumber.Bind(wx.EVT_TEXT, self.checkSerialValidity)
		
	def AllowBack(self):
		return True
		
	def AllowNext(self):
		return False
		
	def skipPage(self, e):
		if self.skipConfig.GetValue():
			self.GetParent().FindWindowById(wx.ID_FORWARD).Enable()
			self.configurePrinterButton.Disable()
			# If the user decides to skip configuration, but has already attempted configuration,
			# delete the printer from the archive
			serial = self.serialNumber.GetValue()
			profile.OctoPrintAPIRemoveSerial(serial)
		else:
			self.GetParent().FindWindowById(wx.ID_FORWARD).Disable()
			self.configurePrinterButton.Enable()
			self.passCheck()
		self.errorMessageln1.SetLabel('')
		

	def checkSerialValidity(self, e):
		id = self.serialNumber.GetValue()
		validityCheck =  self.inputCheck.verifySerial(id)
		
		if validityCheck == 0:
			self.validSerial = True
			self.errorMessageln1.SetLabel("")
			self.configurePrinterButton.Enable()
		else:
			self.configurePrinterButton.Disable()

	def unSavePrinter(self):
		profile.OctoPrintAPIRemoveSerial(self.serialNumber)
	
	# Key check
	def checkKeyValidity(self, e):
		key = self.APIKey.GetValue()
		keyLength = len(key)
		
		validityCheck = self.inputCheck.verifyKey(key)
		if validityCheck == 0:
			self.validKey = True
			self.errorMessageln1.SetLabel("")
		else:
			self.validKey = False
	#		self.errorMessageln0.SetLabel("Error")
			self.errorMessageln1.SetForegroundColour('Red')
			self.errorMessageln1.SetLabel("API key consists of 32 characters")

		self.passCheck()

	def passCheck(self):
		if self.validSerial == True and self.validKey == True and not self.skipConfig.GetValue():
			self.saveInfo = True
		else:
			self.saveInfo = False
			
	def attemptConfiguration(self, e):
		key = self.APIKey.GetValue()
		serial = self.serialNumber.GetValue()
		saveInfo = self.saveInfo
		self.configurationAttemptedOnce = True
		self.errorMessageln1.SetForegroundColour('Blue')
		self.errorMessageln1.SetLabel("Configuring...")
		self.configurePrinterButton.Disable()

		thread = printerConnect.ConfirmCredentials(self, True, key, serial, self.errorMessageln1)
		thread.start()
		
	def StoreData(self):
		serial = self.serialNumber.GetValue()
		key = self.APIKey.GetValue()
		
		
		if 	self.skipConfig.GetValue() == True and self.configurationAttemptedOnce == True:
			if profile.configExists() is not None and serial is None and key is not None:
				profile.OctoPrintAPIRemoveSerial(serial)
				print "Config does not exist yet."
		else:
			self.GetParent().FindWindowById(wx.ID_FORWARD).Enable()

class TAMFirstPrint(InfoPage):
	def __init__(self, parent):
		super(TAMFirstPrint, self).__init__(parent, _("Your First Print"))
		self.AddLogo()
		self.AddText("Click 'Finish' and Cura Type A will open and open an example model which you can use to become more familiar with the application.")
		self.AddHiddenSeperator(1)
		saveAndUploadImage = resources.getPathForImage('readyToGoPage.png')
		self.AddImage(saveAndUploadImage)
		gettingStarted = "Getting Started Page"
		self.AddHiddenSeperator(1)
		self.AddText("When you are ready to print, click the 'Save' or 'Upload' icon to save and start printing your 3D models.")
		self.AddHiddenSeperator(1)
		self.AddCenteredText("For more information, visit our Getting Started page:")
		self.AddHyperlink("typeamachines.com/gettingstarted", "http://www.typeamachines.com/gettingstarted")
		

class ConfigWizard(wx.wizard.Wizard):
	def __init__(self, parent, addNew = False):
		super(ConfigWizard, self).__init__(None, -1, _("Configuration Wizard"), style=wx.STAY_ON_TOP | wx.DEFAULT_DIALOG_STYLE)
		
		self.addNew = addNew
		# Get the number of the current machine and label it as the old index
		self._old_machine_index = int(profile.getPreferenceFloat('active_machine'))
		if self.addNew:
			profile.setActiveMachine(profile.getMachineCount())
		
		self.dataToSaveList = []
		self.Bind(wx.wizard.EVT_WIZARD_PAGE_CHANGED, self.OnPageChanged)
		self.Bind(wx.wizard.EVT_WIZARD_PAGE_CHANGING, self.OnPageChanging)
		self.Bind(wx.wizard.EVT_WIZARD_CANCEL, self.OnCancel)
		self.Bind(wx.wizard.EVT_WIZARD_FINISHED, self.OnFinished)

		self.firstInfoPage = FirstInfoPage(self, addNew)
		self.machineSelectPage = MachineSelectPage(self)
		self.tamReadyPage = TAMReadyPage(self)
		self.TAM_octoprint_config = TAMOctoPrintInfo(self)
		self.TAM_select_options = TAMSelectOptions(self)
		self.TAM_first_print = TAMFirstPrint(self)
		
		self.TAM_absolute_infill = TAMAbsoluteInfillPage(self)
		self.TAM_3D_infill = TAM3DInfillPage(self)
		self.TAM_heated_bed_warning = TAMHeatedBedWarning(self)
		
		wx.wizard.WizardPageSimple.Chain(self.firstInfoPage, self.machineSelectPage)
		wx.wizard.WizardPageSimple.Chain(self.machineSelectPage, self.TAM_octoprint_config)
		wx.wizard.WizardPageSimple.Chain(self.TAM_octoprint_config, self.tamReadyPage)
		wx.wizard.WizardPageSimple.Chain(self.tamReadyPage, self.TAM_absolute_infill)
		wx.wizard.WizardPageSimple.Chain(self.TAM_absolute_infill, self.TAM_3D_infill)
		wx.wizard.WizardPageSimple.Chain(self.TAM_3D_infill, self.TAM_heated_bed_warning)
		wx.wizard.WizardPageSimple.Chain(self.TAM_heated_bed_warning, self.TAM_first_print)

		self.FitToPage(self.firstInfoPage)
		self.GetPageAreaSizer().Add(self.firstInfoPage)

		self.RunWizard(self.firstInfoPage)
		self.Destroy()

	def OnPageChanging(self, e):
		self.dataToSaveList.append(e.GetPage().StoreData())

	def OnPageChanged(self, e):
		if e.GetPage().AllowNext():
			self.FindWindowById(wx.ID_FORWARD).Enable()
		else:
			self.FindWindowById(wx.ID_FORWARD).Disable()
		if e.GetPage().AllowBack():
			self.FindWindowById(wx.ID_BACKWARD).Enable()
		else:
			self.FindWindowById(wx.ID_BACKWARD).Disable()

	def OnCancel(self, e):
		print e
		if self.addNew == True:
			profile.setActiveMachine(self._old_machine_index)
		else:
			profile.putPreference('configured', 'False')
			preferences = profile.getPreferencePath()
			with open(preferences, "w+"):
				pass
				
	def OnFinished(self, e):
		for x in self.dataToSaveList:
			x
		profile.putPreference('configured', 'True')
				
	def disableNext(self):
		self.FindWindowById(wx.ID_FORWARD).Disable()
