 Sharing is fun!

The below models are shared under the CC BY-NC 3.0 license (http://creativecommons.org/licenses/by-nc/3.0/), and can be shared and used freely for none-commercial purposes as long as attribution is added:

* FirstPrintCone.stl: Type A Machines
